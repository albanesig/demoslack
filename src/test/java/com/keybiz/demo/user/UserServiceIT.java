package com.keybiz.demo.user;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class UserServiceIT {

    /**
     * Configurating tests might have issue if you use @RequiredArgsConstructor
     * on wich layer you are doing it
     */

    @TestConfiguration
    static class UserServiceContextConfiguration {

        @Bean
        public IUserService iUserService() {
            return new UserService();
        }
    }

    @Autowired
    private IUserService iUserService;

    @MockBean
    private UserRepository userRepository;

    @Before
    public void setUp() {
        User newUser = new User();
        Mockito.when(userRepository.getOne(newUser.getId()))
                .thenReturn(newUser);
    }

    @Test
    public void whenValidId_thenUserShouldBeFound() {
        Long id = Long.valueOf(1);
        Optional<User> found = iUserService.getUserById(id);
        found.ifPresent(user -> assertThat(user.getId())
                .isEqualTo(id));
    }
}
