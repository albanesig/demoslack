package com.keybiz.demo.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIT {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindByPrimaryKey_returnUser(){
        // given
        User newUser = new User();
        entityManager.persist(newUser);
        entityManager.flush();
        // when
        User found = userRepository.getOne(newUser.getId());
        // then
        assertThat(found.getId()).isEqualTo(newUser.getId());
    }
}