package com.keybiz.demo.message;

import com.keybiz.demo.chat.ChatRepository;
import com.keybiz.demo.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MessageService implements IMessageService {

    private final MessageRepository messageRepository;
    private final ChatRepository chatRepository;
    private final UserRepository userRepository;

    @Override
    public MessageDTO saveMessage(MessageDTO messageDTO) {
        Message message = new Message();
        message.setId(messageDTO.getId());
        message.setText(messageDTO.getText());
        message.setChat(chatRepository.findById(messageDTO.getChat()).get());
        message.setUser(userRepository.findById(messageDTO.getUser()).get());
        messageRepository.save(message);
        return MessageHelper.convertToDTO(message);
    }

    @Override
    public Optional<Message> getMessageById(Long idMessage) {
        return messageRepository.findById(idMessage);
    }

    @Override
    public Collection<Message> getAllMessages() {
        return messageRepository.findAll();
    }

}
