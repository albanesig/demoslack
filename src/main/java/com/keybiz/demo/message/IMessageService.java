package com.keybiz.demo.message;

import java.util.Collection;
import java.util.Optional;

public interface IMessageService {

    MessageDTO saveMessage(MessageDTO messageDTO);

    Optional<Message> getMessageById(Long idMessage);

    Collection<Message> getAllMessages();
}
