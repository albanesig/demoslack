package com.keybiz.demo.message;

import com.keybiz.demo.chat.Chat;
import com.keybiz.demo.user.User;
import lombok.Data;

import javax.persistence.*;


@Data
@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Chat chat;

    @ManyToOne
    private User user;

    private String text;

}

