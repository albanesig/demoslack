package com.keybiz.demo.message;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "message")
@RequiredArgsConstructor
public class MessageController {

    private final IMessageService IMessageService;

    @PostMapping
    public ResponseEntity<?> saveMessage(@RequestBody MessageDTO messageDTO){
        return ResponseEntity.ok(IMessageService.saveMessage(messageDTO));
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> getMessage(@PathVariable("id") Long id){
        Optional<Message> dtoOptional = IMessageService.getMessageById(id);
        if(!dtoOptional.isPresent()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(MessageHelper.convertToDTO(dtoOptional.get()));
    }

    @GetMapping
    public ResponseEntity<?> getMessages(){
        return ResponseEntity.ok().body(MessageHelper.convertToDTO(IMessageService.getAllMessages()));
    }

}
