package com.keybiz.demo.message;

import lombok.Data;

@Data
public class MessageDTO {

    private Long id;
    private String text;
    private Long chat;
    private Long user;


}
