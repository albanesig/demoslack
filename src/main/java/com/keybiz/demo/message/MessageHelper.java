package com.keybiz.demo.message;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class MessageHelper {

    public static MessageDTO convertToDTO(Message message){
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setId(message.getId());
        messageDTO.setText(message.getText());
        messageDTO.setChat(message.getChat().getId());
        messageDTO.setUser(message.getUser().getId());
        return messageDTO;
    }

    public static Collection<MessageDTO> convertToDTO(Collection<Message> all) {
        return all
                .stream()
                .map(MessageHelper::convertToDTO)
                .collect(Collectors.toList());
    }

}
