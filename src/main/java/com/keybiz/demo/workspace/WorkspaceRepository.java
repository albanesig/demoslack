package com.keybiz.demo.workspace;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface WorkspaceRepository extends JpaRepository<Workspace, Long> {

    @Query(value = "SELECT w FROM Workspace w WHERE w.name = ?1")
    Workspace getByName(String name);
}
