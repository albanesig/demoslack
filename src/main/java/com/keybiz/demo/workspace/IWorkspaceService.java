package com.keybiz.demo.workspace;


import java.util.Collection;
import java.util.Optional;

public interface IWorkspaceService {

    WorkspaceDTO saveWorkspace(WorkspaceDTO userDTO);

    Optional<Workspace> getWorkspaceById(Long idWorkspace);

    Collection<Workspace> getAllWorkspaces();

    /**
     * This method adds a list of users into a specific Workspace
     *
     * @param workspaceDTO
     * @return
     */
    WorkspaceDTO addUsersInWorkspace(WorkspaceDTO workspaceDTO);

    WorkspaceDTO getByName(String name);

    Collection<WorkspaceDTO> findWorkspacesByUserId(Long userId);
}
