package com.keybiz.demo.workspace;

import lombok.Data;

import java.util.Collection;

@Data
public class WorkspaceDTO {


    private Long id;
    private String name;
    private Collection<Long> wsUsers;
    private Collection<Long> wsTasks;
    private Collection<Long> wsChats;

}
