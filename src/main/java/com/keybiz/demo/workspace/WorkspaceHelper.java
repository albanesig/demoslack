package com.keybiz.demo.workspace;

import com.keybiz.demo.chat.Chat;
import com.keybiz.demo.task.Task;
import com.keybiz.demo.user.User;
import com.keybiz.demo.userWorkspace.UserWorkspace;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class WorkspaceHelper {

    public static WorkspaceDTO convertToDTO(Workspace workspace){
        WorkspaceDTO workspaceDTO = new WorkspaceDTO();
        workspaceDTO.setId(workspace.getId());
        workspaceDTO.setName(workspace.getName());
        if(workspace.getUserWorkspaces() != null){
            workspaceDTO.setWsUsers(workspace.getUserWorkspaces().stream()
                    .map(UserWorkspace::getUser)
                    .map(User::getId)
                    .collect(Collectors.toList()));
        }
        if(workspace.getWsChats() != null){
            workspaceDTO.setWsChats(workspace.getWsChats().stream().map(Chat::getId).collect(Collectors.toList()));
        }
        if(workspace.getWsTasks() != null){
            workspaceDTO.setWsTasks(workspace.getWsTasks().stream().map(Task::getId).collect(Collectors.toList()));
        }
        return workspaceDTO;
    }

    public static Collection<WorkspaceDTO> convertToDTO(Collection<Workspace> all) {
        return all
                .stream()
                .map(WorkspaceHelper::convertToDTO)
                .collect(Collectors.toList());
    }
}
