package com.keybiz.demo.workspace;

import com.keybiz.demo.user.User;
import com.keybiz.demo.user.UserRepository;
import com.keybiz.demo.userWorkspace.UserWorkspaceDTO;
import com.keybiz.demo.userWorkspace.UserWorkspaceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class WorkspaceService implements IWorkspaceService {

    private final WorkspaceRepository workspaceRepository;
    private final UserWorkspaceService userWorkspaceService;
    private final UserRepository userRepository;

    @Override
    public WorkspaceDTO saveWorkspace(WorkspaceDTO workspaceDTO) {
        Workspace workspace = new Workspace();
        workspace.setId(workspaceDTO.getId());
        workspace.setName(workspaceDTO.getName());
        workspaceRepository.save(workspace);

        return WorkspaceHelper.convertToDTO(workspace);
    }

    @Override
    public WorkspaceDTO addUsersInWorkspace(WorkspaceDTO workspaceDTO) {
        UserWorkspaceDTO userWorkspaceDTO = new UserWorkspaceDTO();
        userWorkspaceDTO.setWorkspaceId(workspaceDTO.getId());
        for(Long idUser : workspaceDTO.getWsUsers()){
            userWorkspaceDTO.setUserId(idUser);
            userWorkspaceService.saveUserWorkspace(userWorkspaceDTO);
        }
        return WorkspaceHelper.convertToDTO(workspaceRepository.getOne(workspaceDTO.getId()));
    }

    @Override
    public Optional<Workspace> getWorkspaceById(Long idWorkspace) {
        return workspaceRepository.findById(idWorkspace);
    }

    @Override
    public Collection<Workspace> getAllWorkspaces() {
        return workspaceRepository.findAll();
    }

    @Override
    public WorkspaceDTO getByName(String name) {
        return WorkspaceHelper.convertToDTO(workspaceRepository.getByName(name));
    }

    @Override
    public Collection<WorkspaceDTO> findWorkspacesByUserId(Long userId) {
        User user = userRepository.getOne(userId);
        return WorkspaceHelper.convertToDTO(userWorkspaceService.findWorkspacesByUserId(user));
    }

}
