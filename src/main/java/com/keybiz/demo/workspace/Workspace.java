package com.keybiz.demo.workspace;


import com.keybiz.demo.chat.Chat;
import com.keybiz.demo.task.Task;
import com.keybiz.demo.userWorkspace.UserWorkspace;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;


@Data
@Entity
public class Workspace implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "workspace")
    private Collection<UserWorkspace> userWorkspaces;


    @OneToMany(mappedBy = "workspace")
    private Collection<Task> wsTasks;

    @OneToMany(mappedBy = "workspace")
    private Collection<Chat> wsChats;

}
