package com.keybiz.demo.workspace;

import com.keybiz.demo.user.IUserService;
import com.keybiz.demo.user.User;
import com.keybiz.demo.user.UserHelper;
import com.keybiz.demo.userWorkspace.IUserWorkspaceService;
import com.keybiz.demo.userWorkspace.UserWorkspace;
import com.keybiz.demo.userWorkspace.UserWorkspaceHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "workspace")
@RequiredArgsConstructor
public class WorkspaceController {

    private final IWorkspaceService IWorkspaceService;
    private final IUserService iUserService;
    private final IUserWorkspaceService iUserWorkspaceService;

    @PostMapping(value = "{userId}")
    public ResponseEntity<?> saveWorkspace(@RequestBody WorkspaceDTO workspaceDTO, @PathVariable Long userId){
        Optional<User> user = iUserService.getUserById(userId);
        if(!user.isPresent()){
            return ResponseEntity.notFound().build();
        }
        UserWorkspace userWorkspace = new UserWorkspace();
        userWorkspace.setUser(user.get());
        IWorkspaceService.saveWorkspace(workspaceDTO);
        userWorkspace.setWorkspace(IWorkspaceService.getByName(workspaceDTO.getName()));
        iUserWorkspaceService.saveUserWorkspace(UserWorkspaceHelper.convert(userWorkspace));
        return ResponseEntity.ok(workspaceDTO);
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> getWorkspace(@PathVariable("id") Long id){
        Optional<Workspace> dtoOptional = IWorkspaceService.getWorkspaceById(id);
        if(!dtoOptional.isPresent()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(WorkspaceHelper.convertToDTO(dtoOptional.get()));
    }

    @GetMapping
    public ResponseEntity<?> getWorkspaces(){
        return ResponseEntity.ok().body(WorkspaceHelper.convertToDTO(IWorkspaceService.getAllWorkspaces()));
    }

    @PostMapping(value = "users")
    public ResponseEntity<?> addUsersInWorkspace(@RequestBody WorkspaceDTO workspaceDTO){
        IWorkspaceService.addUsersInWorkspace(workspaceDTO);
        return ResponseEntity.ok(workspaceDTO);
    }


    @GetMapping(value = "find/{name}")
    public ResponseEntity<?> getWorkspaceByName(@PathVariable String name){
        return ResponseEntity.ok().body(IWorkspaceService.getByName(name));
    }

    @GetMapping(value = "find/user/{id}")
    public ResponseEntity<?> findWorkspacesByUserId(@PathVariable Long id){
        Optional<User> optionalUser = iUserService.getUserById(id);
        if(!optionalUser.isPresent()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(IWorkspaceService.findWorkspacesByUserId(id));
    }
}
