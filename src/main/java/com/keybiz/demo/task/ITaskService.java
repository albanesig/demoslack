package com.keybiz.demo.task;

import java.util.Collection;
import java.util.Optional;

public interface ITaskService {


    TaskDTO saveTask(TaskDTO taskDTO);

    Optional<Task> getTaskById(Long idTask);

    Collection<Task> getAllTasks();

}
