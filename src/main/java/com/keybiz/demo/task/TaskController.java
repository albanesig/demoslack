package com.keybiz.demo.task;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "task")
@RequiredArgsConstructor
public class TaskController {

    private final ITaskService ITaskService;

    @PostMapping
    public ResponseEntity<?> saveTask(@RequestBody TaskDTO taskDTO){
        return ResponseEntity.ok(ITaskService.saveTask(taskDTO));
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> getTask(@PathVariable("id") Long id){
        Optional<Task> dtoOptional = ITaskService.getTaskById(id);
        if(!dtoOptional.isPresent()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(TaskHelper.convertToDTO(dtoOptional.get()));
    }

    @GetMapping
    public ResponseEntity<?> getTasks(){
        return ResponseEntity.ok().body(TaskHelper.convertToDTO(ITaskService.getAllTasks()));
    }
}
