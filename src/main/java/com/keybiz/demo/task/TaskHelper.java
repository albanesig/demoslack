package com.keybiz.demo.task;

import com.keybiz.demo.user.User;
import com.keybiz.demo.userTask.UserTask;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class TaskHelper {

    public static TaskDTO convertToDTO(Task task){
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setTitle(task.getTitle());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setWorkspace(task.getWorkspace().getId());
        if(task.getUserTasks() != null) {
            taskDTO.setUsers(task.getUserTasks().stream().map( UserTask::getId).collect(Collectors.toList()));
        }
        return taskDTO;
    }

    public static Collection<TaskDTO> convertToDTO(Collection<Task> all) {
        return all
                .stream()
                .map(TaskHelper::convertToDTO)
                .collect(Collectors.toList());
    }
}
