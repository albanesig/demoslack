package com.keybiz.demo.task;


import com.keybiz.demo.userTask.UserTask;
import com.keybiz.demo.workspace.Workspace;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(mappedBy = "task")
    private Collection<UserTask> userTasks;

    @ManyToOne
    private Workspace workspace;

    private String title;

    private String description;
}
