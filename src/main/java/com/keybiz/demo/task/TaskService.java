package com.keybiz.demo.task;

import com.keybiz.demo.workspace.Workspace;
import com.keybiz.demo.workspace.WorkspaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TaskService implements ITaskService {

    private final TaskRepository taskRepository;
    private final WorkspaceRepository workspaceRepository;

    @Override
    public TaskDTO saveTask(TaskDTO taskDTO) {
        Task task = new Task();
        task.setId(taskDTO.getId());
        task.setTitle(taskDTO.getTitle());
        task.setDescription(taskDTO.getDescription());
        task.setWorkspace(getWorkspace(taskDTO.getWorkspace()));
//        if(taskDTO.getUsers() != null) {
//            task.setUserTasks(convert(taskDTO.getUsers(), "User"));
//        }
        taskRepository.save(task);
        return TaskHelper.convertToDTO(task);
    }

    private Workspace getWorkspace(Long id){
        return workspaceRepository.findById(id).get();
    }

    @Override
    public Optional<Task> getTaskById(Long idTask) {
        return taskRepository.findById(idTask);
    }

    @Override
    public Collection<Task> getAllTasks() {
        return taskRepository.findAll();
    }
}
