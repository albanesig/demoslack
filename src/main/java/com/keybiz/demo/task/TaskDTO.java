package com.keybiz.demo.task;

import lombok.Data;

import java.util.Collection;

@Data
public class TaskDTO {

    private Long id;
    private String title;
    private String description;
    private Long workspace;
    private Collection<Long> users;

}
