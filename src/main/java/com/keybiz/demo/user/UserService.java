package com.keybiz.demo.user;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;


@Service
public class UserService implements IUserService {

    /**
     * Configurating tests might have issue if you use @RequiredArgsConstructor
     * on wich layer you are doing it
     */
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDTO saveUser(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setRole(userDTO.getRole());
        userRepository.save(user);
        return UserHelper.convertToDTO(user);
    }

    @Override
    public Optional<User> getUserById(Long idUser) {
        return userRepository.findById(idUser);
    }

    @Override
    public Collection<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public UserDTO findByEmailPassword(String email, String password) {
        return UserHelper.convertToDTO(userRepository.findByEmailPassword(email, password));
    }

    @Override
    public Collection<UserDTO> findUsersByWorkspaceId(Long workspaceIds) {
        return null;
    }
}
