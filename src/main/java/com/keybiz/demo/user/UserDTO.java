package com.keybiz.demo.user;

import lombok.Data;

import java.util.Collection;

@Data
public class UserDTO {


    private Long id;
    private String email;
    private String password;
    private String role;
    private Collection<Long> tasks;
    private Collection<Long> workspaces;
    private Collection<Long> chats;
    private Collection<Long> messages;

}
