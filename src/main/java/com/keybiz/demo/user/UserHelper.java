package com.keybiz.demo.user;


import com.keybiz.demo.userChat.UserChat;
import com.keybiz.demo.userWorkspace.UserWorkspace;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class UserHelper {

    public static UserDTO convertToDTO(User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setEmail(user.getEmail());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());
        if(user.getUserWorkspaces() != null){
            userDTO.setWorkspaces(user.getUserWorkspaces().stream().map(UserWorkspace::getId).collect(Collectors.toList()));
        }
        if(user.getUserChats() != null){
            userDTO.setChats(user.getUserChats().stream().map(UserChat::getId).collect(Collectors.toList()));
        }
        return userDTO;
    }

    public static Collection<UserDTO> convertToDTO(Collection<User> all) {
        return all
                .stream()
                .map(UserHelper::convertToDTO)
                .collect(Collectors.toList());
    }

}
