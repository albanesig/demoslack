package com.keybiz.demo.user;


import java.util.Collection;
import java.util.Optional;

public interface IUserService {

    UserDTO saveUser(UserDTO userDTO);

    Optional<User> getUserById(Long idUser);

    Collection<User> getAllUsers();

    UserDTO findByEmailPassword(String email, String password);

    Collection<UserDTO> findUsersByWorkspaceId(Long workspaceIds);
}
