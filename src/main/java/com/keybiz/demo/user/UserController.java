package com.keybiz.demo.user;


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin( origins = "*")
@RestController
@RequestMapping(value = "user")
@RequiredArgsConstructor
public class UserController {

    private final IUserService IUserService;

    @PostMapping
    public ResponseEntity<?> saveUser(@RequestBody UserDTO userDTO){
        return ResponseEntity.ok(IUserService.saveUser(userDTO));
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> getUser(@PathVariable("id") Long id){
        Optional<User> dtoOptional = IUserService.getUserById(id);
        if(!dtoOptional.isPresent()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(UserHelper.convertToDTO(dtoOptional.get()));
    }

    @GetMapping
    public ResponseEntity<?> getUsers(){
        return ResponseEntity.ok().body(UserHelper.convertToDTO(IUserService.getAllUsers()));
    }

    @GetMapping(value = "find/{email}/{password}")
    public ResponseEntity<?> findByEmailPassword(@PathVariable String email,@PathVariable String password){
        return ResponseEntity.ok().body(IUserService.findByEmailPassword(email, password));
    }

}
