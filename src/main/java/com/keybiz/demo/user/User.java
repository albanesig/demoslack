package com.keybiz.demo.user;


import com.keybiz.demo.message.Message;
import com.keybiz.demo.userChat.UserChat;
import com.keybiz.demo.userTask.UserTask;
import com.keybiz.demo.userWorkspace.UserWorkspace;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Data
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String email;
    private String password;
    private String role;

    @OneToMany(mappedBy = "user")
    private Collection<UserTask> userTasks;

    @OneToMany(mappedBy = "user")
    private Collection<UserWorkspace> userWorkspaces;

    @OneToMany(mappedBy = "user")
    private Collection<UserChat> userChats;

    @OneToMany(mappedBy = "user")
    private Collection<Message> messages;

}
