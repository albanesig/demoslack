package com.keybiz.demo.userWorkspace;


import com.keybiz.demo.user.User;
import com.keybiz.demo.workspace.Workspace;

import java.util.Collection;
import java.util.Optional;

public interface IUserWorkspaceService {

    UserWorkspaceDTO saveUserWorkspace(UserWorkspaceDTO userWorkspaceDTO);

    Collection<Workspace> findWorkspacesByUserId(User user);

    Collection<Long> findUsersByWorkspaceId(Long workspaceId);

    Optional<UserWorkspace> getUserWorkspaceById(Long id);

    Collection<UserWorkspace> getAllUserWorkspaces();

    boolean deleteById(Long id);


}
