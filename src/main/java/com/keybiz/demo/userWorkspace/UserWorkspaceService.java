package com.keybiz.demo.userWorkspace;

import com.keybiz.demo.user.User;
import com.keybiz.demo.user.UserRepository;
import com.keybiz.demo.workspace.Workspace;
import com.keybiz.demo.workspace.WorkspaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserWorkspaceService implements IUserWorkspaceService{

    private final UserWorkspaceRepository userWorkspaceRepository;
    private final WorkspaceRepository workspaceRepository;
    private final UserRepository userRepository;

    @Override
    public UserWorkspaceDTO saveUserWorkspace(UserWorkspaceDTO userWorkspaceDTO) {
        UserWorkspace userWorkspace = new UserWorkspace();
        userWorkspace.setWorkspace(workspaceRepository.getOne(userWorkspaceDTO.getWorkspaceId()));
        userWorkspace.setUser(userRepository.getOne(userWorkspaceDTO.getUserId()));
        userWorkspaceRepository.save(userWorkspace);
        return UserWorkspaceHelper.convert(userWorkspace);
    }

    @Override
    public Collection<Workspace> findWorkspacesByUserId(User user) {
        return userWorkspaceRepository.findWorkspacesByUserId(user);
    }

    @Override
    public Collection<Long> findUsersByWorkspaceId(Long workspaceId) {
        return userWorkspaceRepository.findUsersByWorkspaceId(workspaceId);
    }

    @Override
    public Optional<UserWorkspace> getUserWorkspaceById(Long id) {
        return userWorkspaceRepository.findById(id);
    }

    @Override
    public Collection<UserWorkspace> getAllUserWorkspaces() {
        return userWorkspaceRepository.findAll();
    }

    @Override
    public boolean deleteById(Long id) {
        Optional<UserWorkspace> userWorkspace = userWorkspaceRepository.findById(id);
        if(!userWorkspace.isPresent()){
            return false;
        }
        userWorkspaceRepository.deleteById(id);
        return true;
    }

    private UserWorkspace convertToEntity(UserWorkspaceDTO userWorkspaceDTO){
        UserWorkspace userWorkspace = new UserWorkspace();
        userWorkspace.setId(userWorkspaceDTO.getId());
        userWorkspace.setWorkspace(workspaceRepository.getOne(userWorkspaceDTO.getWorkspaceId()));
        userWorkspace.setUser(userRepository.getOne(userWorkspaceDTO.getId()));
        return userWorkspace;
    }
}
