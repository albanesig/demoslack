package com.keybiz.demo.userWorkspace;

import com.keybiz.demo.user.User;
import com.keybiz.demo.workspace.Workspace;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class UserWorkspace {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Workspace workspace;
}
