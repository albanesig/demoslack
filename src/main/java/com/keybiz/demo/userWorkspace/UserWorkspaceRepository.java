package com.keybiz.demo.userWorkspace;

import com.keybiz.demo.user.User;
import com.keybiz.demo.workspace.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface UserWorkspaceRepository extends JpaRepository<UserWorkspace, Long> {

    @Query(value = "SELECT uw.workspace FROM UserWorkspace uw WHERE uw.user = ?1")
    Collection<Workspace> findWorkspacesByUserId(User user);

    @Query(value = "SELECT uw.user FROM UserWorkspace uw WHERE uw.workspace = ?1")
    Collection<Long> findUsersByWorkspaceId(Long workspaceId);

}
