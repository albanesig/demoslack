package com.keybiz.demo.userWorkspace;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class UserWorkspaceHelper {

    public static UserWorkspaceDTO convert(UserWorkspace userWorkspace){
        UserWorkspaceDTO userWorkspaceDTO = new UserWorkspaceDTO();
        userWorkspaceDTO.setId(userWorkspace.getId());
        userWorkspaceDTO.setWorkspaceId(userWorkspace.getWorkspace().getId());
        userWorkspaceDTO.setUserId(userWorkspace.getUser().getId());
        return userWorkspaceDTO;
    }

    public static Collection<UserWorkspaceDTO> convert(Collection<UserWorkspace> all){
        return all.stream().map(UserWorkspaceHelper::convert).collect(Collectors.toList());
    }
}
