package com.keybiz.demo.userWorkspace;

import lombok.Data;

@Data
public class UserWorkspaceDTO {

    private Long id;
    private Long userId;
    private Long workspaceId;
}
