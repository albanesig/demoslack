package com.keybiz.demo.userChat;


import com.keybiz.demo.chat.Chat;
import com.keybiz.demo.chat.ChatRepository;
import com.keybiz.demo.user.User;
import com.keybiz.demo.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserChatService implements IUserChatService{

    private final UserChatRepository userChatRepository;
    private final ChatRepository chatRepository;
    private final UserRepository userRepository;

    @Override
    public UserChatDTO saveUserChat(UserChatDTO userChatDTO) {
        UserChat userChat = new UserChat();
        userChat.setChat(chatRepository.getOne(userChatDTO.getChatU()));
        userChat.setUser(userRepository.getOne(userChatDTO.getUserC()));
        userChatRepository.save(userChat);
        return UserChatHelper.convert(userChat);
    }

    @Override
    public Collection<Chat> findChatsByUserId(User user) {
        return userChatRepository.findChatsByUserId(user);
    }

    @Override
    public Optional<UserChat> getUserChatById(Long id) {
        return userChatRepository.findById(id);
    }

    @Override
    public Collection<UserChat> getAllUserChats() {
        return userChatRepository.findAll();
    }

    @Override
    public boolean deleteById(Long id) {
        Optional<UserChat> userChat = userChatRepository.findById(id);
        if(!userChat.isPresent()){
            return false;
        }
        userChatRepository.deleteById(id);
        return true;
    }

    private UserChat convertToEntity(UserChatDTO userChatDTO){
        UserChat userChat = new UserChat();
        userChat.setId(userChatDTO.getId());
        userChat.setChat(chatRepository.getOne(userChatDTO.getChatU()));
        userChat.setUser(userRepository.getOne(userChatDTO.getId()));
        return userChat;
    }
}