package com.keybiz.demo.userChat;

import com.keybiz.demo.chat.Chat;
import com.keybiz.demo.user.User;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class UserChat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Chat chat;
}
