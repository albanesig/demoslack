package com.keybiz.demo.userChat;


import com.keybiz.demo.chat.Chat;
import com.keybiz.demo.user.User;

import java.util.Collection;
import java.util.Optional;

public interface IUserChatService {

    UserChatDTO saveUserChat(UserChatDTO userChat);

    Optional<UserChat> getUserChatById(Long id);

    Collection<UserChat> getAllUserChats();

    boolean deleteById(Long id);

    Collection<Chat> findChatsByUserId(User user);
}

