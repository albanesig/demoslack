package com.keybiz.demo.userChat;

import lombok.Data;

@Data
public class UserChatDTO {

    private Long id;
    private Long userC;
    private Long chatU;
}
