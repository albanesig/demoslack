package com.keybiz.demo.userChat;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class UserChatHelper {


    public static UserChatDTO convert(UserChat userChat){
        UserChatDTO userChatDTO = new UserChatDTO();
        userChatDTO.setId(userChat.getId());
        userChatDTO.setChatU(userChat.getChat().getId());
        userChatDTO.setUserC(userChat.getUser().getId());
        return userChatDTO;
    }

    public static Collection<UserChatDTO> convert(Collection<UserChat> all){
        return all.stream().map(UserChatHelper::convert).collect(Collectors.toList());
    }
}
