package com.keybiz.demo.userChat;

import com.keybiz.demo.chat.Chat;
import com.keybiz.demo.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;

public interface UserChatRepository extends JpaRepository<UserChat, Long> {

    @Query(value = "SELECT uc.chat FROM UserChat uc WHERE uc.user = ?1")
    Collection<Chat> findChatsByUserId(User USer);

    @Query(value = "SELECT uc.user FROM UserChat uc WHERE uc.chat = ?1")
    Collection<User> findUSersByChatId(Chat chat);
}
