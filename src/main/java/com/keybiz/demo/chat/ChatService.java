package com.keybiz.demo.chat;

import com.keybiz.demo.message.Message;
import com.keybiz.demo.message.MessageRepository;
import com.keybiz.demo.user.User;
import com.keybiz.demo.user.UserRepository;
import com.keybiz.demo.userChat.UserChatDTO;
import com.keybiz.demo.userChat.UserChatService;
import com.keybiz.demo.workspace.Workspace;
import com.keybiz.demo.workspace.WorkspaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ChatService implements IChatService {

    private final ChatRepository chatRepository;
    private final UserRepository userRepository;
    private final WorkspaceRepository workspaceRepository;
    private final MessageRepository messageRepository;

    private final UserChatService userChatService;

    @Override
    public ChatDTO saveChat(ChatDTO chatDTO) {
        Chat chat = new Chat();
        chat.setId(chatDTO.getId());
        chat.setTitle(chatDTO.getTitle());
        chat.setWorkspace(getWorkspace(chatDTO.getWorkspace()));
        if(chatDTO.getWorkspace()!=null){
            chatDTO.setWorkspace(chat.getWorkspace().getId());
        }
        if(chatDTO.getMessages()!=null) {
            chat.setMessageCollection(convert(chatDTO.getMessages(), "Message"));
        }
        chatRepository.save(chat);
        return ChatHelper.convertToDTO(chat);
    }

    @Override
    public ChatDTO addUsersInChat(ChatDTO chatDTO) {
        UserChatDTO userChatDTO = new UserChatDTO();
        userChatDTO.setChatU(chatDTO.getId());
        for(Long idUser : chatDTO.getUsers()){
            userChatDTO.setUserC(idUser);
            userChatService.saveUserChat(userChatDTO);
        }
        return ChatHelper.convertToDTO(chatRepository.getOne(chatDTO.getId()));
    }

    private Workspace getWorkspace(Long id){
        return workspaceRepository.findById(id).get();
    }

    @Override
    public Optional<Chat> getChatById(Long idChat) {
        return chatRepository.findById(idChat);
    }

    @Override
    public Collection<Chat> getAll() {
        return chatRepository.findAll();
    }

    @Override
    public Collection<ChatDTO> findChatsByUserId(Long userId) {
        User user = userRepository.getOne(userId);
        return ChatHelper.convertToDTO(userChatService.findChatsByUserId(user));
    }

    private Collection convert(Collection<Long> longCollection, String entity) {
        Collection objectCollection = null;
        if (entity.equals("Message")) {
            for (Long id : longCollection) {
                Optional<Message> optionalMessage = messageRepository.findById(id);
                optionalMessage.ifPresent(objectCollection::add);
            }
        }
        if (entity.equals("User")) {
            for (Long id : longCollection) {
                Optional<User> optionalUser = userRepository.findById(id);
                optionalUser.ifPresent(objectCollection::add);
            }
        }
        return objectCollection;
    }

}
