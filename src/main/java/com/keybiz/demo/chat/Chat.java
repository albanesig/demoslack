package com.keybiz.demo.chat;

import com.keybiz.demo.message.Message;
import com.keybiz.demo.userChat.UserChat;
import com.keybiz.demo.workspace.Workspace;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Data
@Entity
public class Chat implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;

    @ManyToOne
    private Workspace workspace;

    @OneToMany(mappedBy = "chat")
    private Collection<Message> messageCollection;

    @OneToMany(mappedBy = "chat")
    private Collection<UserChat> userChats;

}
