package com.keybiz.demo.chat;

import java.util.Collection;
import java.util.Optional;

public interface IChatService {

    ChatDTO saveChat(ChatDTO chatDTO);

    ChatDTO addUsersInChat(ChatDTO chatDTO);

    Optional<Chat> getChatById(Long idChat);

    Collection<Chat> getAll();

    Collection<ChatDTO> findChatsByUserId(Long userId);
}
