package com.keybiz.demo.chat;

import lombok.Data;

import java.util.Collection;

@Data
public class ChatDTO {

    private Long id;
    private String title;
    private Long workspace;
    private Collection<Long> users;
    private Collection<Long> messages;

}
