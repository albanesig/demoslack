package com.keybiz.demo.chat;

import com.keybiz.demo.message.Message;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class ChatHelper {

    public static ChatDTO convertToDTO(Chat chat){
        ChatDTO chatDTO = new ChatDTO();
        chatDTO.setId(chat.getId());
        chatDTO.setTitle(chat.getTitle());
        chatDTO.setWorkspace(chat.getWorkspace().getId());
        if(chat.getMessageCollection()!= null){
            chatDTO.setMessages(chat.getMessageCollection().stream().map(Message::getId).collect(Collectors.toList()));
        }
        chatDTO.setUsers(chat.getUserChats().stream().map(c -> c.getUser().getId()).collect(Collectors.toList()));
        return chatDTO;
    }

    public static Collection<ChatDTO> convertToDTO(Collection<Chat> all) {
        return all
                .stream()
                .map(ChatHelper::convertToDTO)
                .collect(Collectors.toList());
    }

}
