package com.keybiz.demo.chat;

import com.keybiz.demo.user.IUserService;
import com.keybiz.demo.user.User;
import com.keybiz.demo.workspace.Workspace;
import com.keybiz.demo.workspace.WorkspaceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(value = "*")
@RestController
@RequestMapping(value = "chat")
@RequiredArgsConstructor
public class ChatController {
    
    private final IChatService IChatService;
    private final IUserService iUserService;
    private final WorkspaceService workspaceService;

    @PostMapping
    public ResponseEntity<?> saveChat(@RequestBody ChatDTO chatDTO){
        return ResponseEntity.ok(IChatService.saveChat(chatDTO));
    }

    @GetMapping(value = "{id}")
    public ResponseEntity<?> getChat(@PathVariable("id") Long id){
        Optional<Chat> dtoOptional = IChatService.getChatById(id);
        if(!dtoOptional.isPresent()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ChatHelper.convertToDTO(dtoOptional.get()));
    }

    @GetMapping
    public ResponseEntity<?> getChats(){
        return ResponseEntity.ok().body(ChatHelper.convertToDTO(IChatService.getAll()));
    }

    @PostMapping(value = "users")
    public ResponseEntity<?> addUser(@RequestBody ChatDTO chatDTO){
        Optional<Workspace> workspace = workspaceService.getWorkspaceById(chatDTO.getId());
        if(!workspace.isPresent()){
            return ResponseEntity.notFound().build();
        }
        IChatService.addUsersInChat(chatDTO);
        return ResponseEntity.ok(chatDTO);
    }

    @GetMapping(value = "find/user/{id}")
    public ResponseEntity<?> findChatsByUserId(@PathVariable Long id){
        Optional<User> optionalUser = iUserService.getUserById(id);
        if(!optionalUser.isPresent()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(IChatService.findChatsByUserId(id));
    }
}
