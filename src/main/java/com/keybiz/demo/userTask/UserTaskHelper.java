package com.keybiz.demo.userTask;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class UserTaskHelper {

    public static UserTaskDTO convert(UserTask userTask){
        UserTaskDTO userTaskDTO = new UserTaskDTO();
        userTaskDTO.setId(userTask.getId());
        userTaskDTO.setUserId(userTask.getUser().getId());
        userTaskDTO.setTaskId(userTask.getTask().getId());
        return userTaskDTO;
    }

    public static Collection<UserTaskDTO> convert(Collection<UserTask> all){
        return all.stream()
                .map(UserTaskHelper::convert)
                .collect(Collectors.toList());
    }
}
