package com.keybiz.demo.userTask;

import java.util.Collection;
import java.util.Optional;

public interface IUserTaskService {

    UserTaskDTO saveUserTask(UserTaskDTO userTaskDTO);

    Optional<UserTask> getUserTaskById(Long id);

    Collection<UserTask> getAllUserTasks();

    boolean deleteById(Long id);

}
