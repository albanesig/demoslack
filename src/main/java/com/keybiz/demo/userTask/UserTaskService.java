package com.keybiz.demo.userTask;

import com.keybiz.demo.task.TaskRepository;
import com.keybiz.demo.user.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserTaskService implements IUserTaskService {

    private final UserTaskRepository userTaskRepository;
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;

    @Override
    public UserTaskDTO saveUserTask(UserTaskDTO userTaskDTO) {
        UserTask userTask = new UserTask();
        userTask.setUser(userRepository.getOne(userTaskDTO.getUserId()));
        userTask.setTask(taskRepository.getOne(userTaskDTO.getTaskId()));
        userTaskRepository.save(userTask);
        return UserTaskHelper.convert(userTask);
    }

    @Override
    public Optional<UserTask> getUserTaskById(Long id) {
        return userTaskRepository.findById(id);
    }

    @Override
    public Collection<UserTask> getAllUserTasks() {
        return userTaskRepository.findAll();
    }

    @Override
    public boolean deleteById(Long id) {
        Optional<UserTask> userTask = userTaskRepository.findById(id);
        if(!userTask.isPresent()){
            return false;
        }
        userTaskRepository.deleteById(id);
        return true;
    }
}
