package com.keybiz.demo.userTask;

import com.keybiz.demo.task.Task;
import com.keybiz.demo.user.User;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class UserTask {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Task task;

}
