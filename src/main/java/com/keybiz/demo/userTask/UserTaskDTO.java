package com.keybiz.demo.userTask;

import lombok.Data;

@Data
public class UserTaskDTO {

    private Long id;
    private Long userId;
    private Long taskId;
}
