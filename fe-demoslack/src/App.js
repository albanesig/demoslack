import React,{Component} from 'react';
import './App.css';

import Container from './components/Container/Container';
import Sidebar from './components/Sidebar/Sidebar';
import Login from './components/Login/Login';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: {
        email: '',
        password: '',
        role: '',
        chats: [],
        workspaces: [],
      },
      login: false

    }
  }

  render(){


    let log = this.state.login;
    let logged = <Login logged={log}/>;

    let workspaces = (
      <div>
        <label>Create new +</label>
      </div>
    );

    return (
      <div className="App">
        <Container>
          <Sidebar
            login={logged}
            workspaces={workspaces}
          />
          Container
        </Container>
      </div>
    )
  }
}

export default App;
