import React from 'react';
import Input from '../Input/Input';
import Form from '../Form/Form';
import User from '../User/User';

const login = (props) => {
  let loginElement = null;

  switch(props.logged) {
    case(false):
      loginElement = <Form>
        Registrati!
        <Input/>
        <Input/>
        <button type="submit">login</button>
      </Form>;
    break;
    case(true):
      loginElement = <div>
        Welcome!
        <User
        email={"gianluca@gmail.com"}
        />
      </div>;
    break;
    default:
      loginElement = <div>
        Click Here for login !
      </div>
  }

  return (
    <div>
      {loginElement}
    </div>
  )
};

export default login;
