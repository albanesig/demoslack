import React from 'react';

const form = (props) => {
  return(
    <form onSubmit={props.submit}>
      <div className="form-group">
        {props.children}
      </div>
    </form>
  )
};

export default form;
