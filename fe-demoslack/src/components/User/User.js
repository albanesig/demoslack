import React from 'react';
import './User.css';

const user = (props) => {
  return(
    <div className="User">
      <p>{props.email}</p>
      <p>{props.role}</p>
    </div>
  )
};

export default user;
