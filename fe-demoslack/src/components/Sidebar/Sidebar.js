import React from 'react';
import './Sidebar.css';

const sidebar = (props) => {
  return(
    <div className="Sidebar">
      <p>{props.login}</p>
      <p>{props.workspaces}</p>
      <p>{props.chats}</p>
    </div>
  )
};

export default sidebar;
